/*!
 * Vitality v1.4.0 (http://themes.startbootstrap.com/vitality-v1.4.0)
 * Copyright 2013-2016 Start Bootstrap Themes
 * To use this theme you must have a license purchased at WrapBootstrap (https://wrapbootstrap.com)
 */

// Load WOW.js on non-touch devices
var isPhoneDevice = "ontouchstart" in document.documentElement;
$(document).ready(function() {
    if (isPhoneDevice) {
        //mobile
    } else {
        //desktop               
        // Initialize WOW.js
        wow = new WOW({
            offset: 50
        })
        wow.init();
    }
    
    $( settings.submit_button_selector ).click( function() {
            var email_value = $( settings.email_input_selector ).val()

            if ( ! email_value ) {
                return setTimeout( function() { handle_error( 'Missing email' ); }, 10 );
            }

            $.ajax({
                type: "POST",
                url: 'https://' + settings.execute_api_domain + '/production/invite',
                contentType: "application/json",
                dataType: 'json',
                data: JSON.stringify( { email : email_value } ),
                success: function( data ) {
                    try {
                        data = JSON.parse( data );
                        if ( data.ok ) {
                            handle_success( 'Invitation sent !' );
                        }
                        else {
                            handle_error( 'API call error: ' + data.error );
                        }
                    }
                    catch ( error ) {
                        handle_error( 'Unknown API return: ' + error );
                    }
                },
                error: function( xhr, status, error ) {
                    handle_error( 'Unknown transport error: ' + status + ( error ? ' ' + error : '' ) );
                }
            });
        } );
});


    var settings = {
        'execute_api_domain' : 'fqnxihcyn8.execute-api.eu-west-1.amazonaws.com',
        'submit_button_selector' : '#mc-embedded-subscribe',
        'email_input_selector' : '#mce-EMAIL',
        'return_message_container_selector' : '#return_message'
    };

    function handle_success( message ) { handle( 'success', message ); }
    function handle_error( message ) { handle( 'error', message ); }

    function handle( message_type, message ) {
        if ( message_type  === "success" ) {
            swal(message_type, message , "success");
        }
        else {
            swal(message_type, message , "error");
        }
    };

(function($) {
    "use strict"; // Start of use strict

    // Smooth Scrolling: Smooth scrolls to an ID on the current page.
    // To use this feature, add a link on your page that links to an ID, and add the .page-scroll class to the link itself. See the docs for more details.
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    // Activates floating label headings for the contact form.
    $("body").on("input propertychange", ".floating-label-form-group", function(e) {
        $(this).toggleClass("floating-label-form-group-with-value", !!$(e.target).val());
    }).on("focus", ".floating-label-form-group", function() {
        $(this).addClass("floating-label-form-group-with-focus");
    }).on("blur", ".floating-label-form-group", function() {
        $(this).removeClass("floating-label-form-group-with-focus");
    });

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function() {
        $('.navbar-toggle:visible').click();
    });

    // Formstone Background - Video Background Settings
    $("header.video").background({
        source: {
            poster: "img/agency/backgrounds/bg-mobile-fallback.jpg",
            mp4: "mp4/LAWS.mp4"
        }
    });

    // Scrollspy: Highlights the navigation menu items while scrolling.
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 51
    })

    // Portfolio Filtering Scripts & Hover Effect
    var filterList = {
        init: function() {

            // MixItUp plugin
            $('#portfoliolist').mixItUp();

        },

    };

    filterList.init();

})(jQuery); // End of use strict
